import {exec} from 'child_process';
import * as ts from 'gulp-typescript';
import {createConnection} from "typeorm";
import {Gulpclass, Task} from "gulpclass";
import * as gulp from 'gulp';
import * as del from 'del';
import {chomp, chunksToLinesAsync} from '@rauschma/stringio'

const tsProject = ts.createProject('tsconfig.json');

const arg: any = (argList => {

    let arg: any = {}, a, opt, thisOpt, curOpt;
    for (a = 0; a < argList.length; a++) {

        thisOpt = argList[a].trim();
        opt = thisOpt.replace(/^\-+/, '');

        if (opt === thisOpt) {

            // argument value
            if (curOpt) arg[curOpt] = opt;
            curOpt = null;

        }
        else {

            // argument name
            curOpt = opt;
            arg[curOpt] = true;

        }

    }

    return arg;

})(process.argv);

async function echoReadable(readable) {
    for await (const line of chunksToLinesAsync(readable)) { // (C)
        console.log('LINE: ' + chomp(line))
    }
}

async function simpleExec(command) {
    const proc = exec(command);
    proc.unref();

    proc.stdout.on('data', (data) => {
        console.log(data);
    });

    proc.stdout.on('error', (err) => {
        console.log(err);
    });

}


@Gulpclass()
export class Gulpfile {

    @Task()
    build() {
        return tsProject.src()
            .pipe(tsProject())
            .js.pipe(gulp.dest('./dist'))
    }

    @Task()
    clean() {
        return del(['./dist/**', './coverage/**'])
    }

    @Task()
    devStart() {
        return simpleExec('./node_modules/.bin/nodemon')
    }

    @Task()
    migrationCreate() {
        const command = './node_modules/.bin/ts-node ./node_modules/.bin/typeorm migration:generate -n ' + arg.n;
        return simpleExec(command);
    }

    @Task()
    migrationRun() {
        let command = './node_modules/.bin/ts-node ./node_modules/typeorm/cli.js migration:run';
        if (arg.c) {
            command += ' -c ' + arg.c
        }
        return simpleExec(command)
    }

    @Task()
    async testDbRenew() {
        // this connection uses very simple connection options - no migrations, no entities
        const connection = await createConnection('clean');
        const queryRunner = connection.createQueryRunner();
        await queryRunner.clearDatabase();
        connection.close();
        arg.c = 'test';
        return this.migrationRun();
    }

    @Task()
    testRun() {
        return simpleExec('yarn test')
    }

}