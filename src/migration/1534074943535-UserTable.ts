import {MigrationInterface, QueryRunner, Table, TableIndex} from "typeorm";

/**
 * @file test.ts
 * @authod shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 08/12/2018
 * (c): 2018
 */

export class UserTable1534074943535 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {

        await queryRunner.createTable(new Table({
            name: 'user',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true
                },
                {
                    name: 'firstName',
                    type: 'varchar'
                },
                {
                    name: 'lastName',
                    type: 'varchar',
                },
                {
                    name: 'phone',
                    type: 'varchar'
                },
                {
                    name: 'phoneCode',
                    type: 'varchar'
                },
                {
                    name: 'phoneNumber',
                    type: 'varchar'
                },
                {
                    name: 'email',
                    type: 'varchar'
                },
                {
                    name: 'isEmailConfirm',
                    type: 'bool'
                },
                {
                    name: 'isBlocked',
                    type: 'bool'
                },
                {
                    name: 'metadata',
                    type: 'jsonb'
                },
            ]
        }), true);

        await queryRunner.createIndex('user', new TableIndex({
            name: 'idx_user_phone',
            columnNames: ['phone']
        }))

    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
