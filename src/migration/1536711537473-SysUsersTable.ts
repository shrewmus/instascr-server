import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class SysUsersTable1536711537473 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP INDEX "idx_user_phone"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "dateCreated" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "user" ADD "dateEdited" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`CREATE SEQUENCE "user_id_seq" OWNED BY "user"."id"`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "id" SET DEFAULT nextval('user_id_seq')`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "UQ_8e1f623798118e629b46a9e6299" UNIQUE ("phone")`);

        await queryRunner.createTable(new Table({
            name: 'sysusers',
            columns: [
                {
                    name: 'id', type: 'uuid', isPrimary: true
                },
                {
                    name: 'sysId', type: 'varchar'
                },
                {
                    name: 'system', type: 'varchar'
                },
                {
                    name: 'systemId', type: 'int', default: 0
                },
                {
                    name: 'firstName', type: 'varchar'
                }

            ]
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "UQ_8e1f623798118e629b46a9e6299"`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "id" DROP DEFAULT`);
        await queryRunner.query(`DROP SEQUENCE "user_id_seq"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "dateEdited"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "dateCreated"`);
        await queryRunner.query(`CREATE INDEX "idx_user_phone" ON "user"("phone") `);
    }

}
