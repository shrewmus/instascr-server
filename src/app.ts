import * as express from 'express'

export class App {
    public express: express.Application;

    constructor() {
        this.express = express()
    }

}
