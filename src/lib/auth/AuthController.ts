/**
 * @file AuthController.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 08/18/2018
 * (c): 2018
 */

import {BaseHttpController, controller, httpPost} from "inversify-express-utils";
import {inject} from "inversify";
import {AuthService} from "./AuthService";


@controller('/v1/auth')
export class AuthController extends BaseHttpController {

    @inject(AuthService) private readonly authService: AuthService;

    @httpPost('/register', )
    public async register(request) {
        console.log('register', arguments);
    }

    @httpPost('/login')
    public async login() {

    }

    public forgotPassword() {
        const err = new Error('forgot method not realized yet');
        err.name = 'ERR_METHOD_NOT_IMPLEMENTED';
        throw err;
    }

}