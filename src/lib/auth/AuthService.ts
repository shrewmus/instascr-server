/**
 * @file AuthService.ts.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 09/05/2018
 * (c): 2018
 */
import {inject, injectable} from 'inversify';
import * as bcrypt from 'bcrypt';
import {UserService} from "../user/UserService";

@injectable()
export class AuthService {

    @inject(UserService) private userService: UserService;

    loginWithEmail(email, password): void {
        this.userService
    }

    loginWithPhone(phone): void {

    }

    async checkPassword(password: string, hash: string): Promise<boolean> {
        return bcrypt.compare(password, hash)
    }

    async hashPassword (password: string): Promise<string> {
        return bcrypt.hash(password, 12)
    }


    registerWithEmail() {
        
    }

}