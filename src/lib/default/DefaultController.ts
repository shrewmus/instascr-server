/**
 * @file DefaultController.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 08/17/2018
 * (c): 2018
 */
import {controller, httpGet, interfaces, response} from "inversify-express-utils";
import * as express from 'express';

@controller('/')
export class DefaultController implements interfaces.Controller {

    @httpGet('/')
    public async getHome(@response() resp: express.Response) {
        return 'test';
    }

}