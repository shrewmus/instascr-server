/**
 * @file CommonMeta.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 09/12/2018
 * (c): 2018
 */

export interface CommonMeta {
    metadata: any,
    dateCreated: any,
    dateEdited:any
}