/**
 * @file Contact.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 08/14/2018
 * (c): 2018
 */

export interface Contact {
    validate(): boolean;
}