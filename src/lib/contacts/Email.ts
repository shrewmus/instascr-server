/**
 * @file Email.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 08/14/2018
 * (c): 2018
 */
import {Contact} from "./Contact";


class Email implements Contact {

    private email: string;

    validate(): boolean {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(this.email);
    }


}