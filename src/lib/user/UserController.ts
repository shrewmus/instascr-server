/**
 * @file UserController.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 08/14/2018
 * (c): 2018
 */
import {controller, httpGet, httpPost, interfaces} from 'inversify-express-utils';
import {Response, Request} from 'express';
import {inject} from "inversify";
import {UserService} from "./UserService";

@controller('/v1/user')
export class UserController implements interfaces.Controller {

    @inject(UserService)
    private userService: UserService;

    @httpGet('/')
    public async getUsers(res: Response ) {
        try {
            return await  this.userService.findAll()
        } catch (e) {
            console.log('Error when getting users')
        }
    }


    @httpPost('/add')
    public addUser(req: Request, res: Response) {

    }

}