/**
 * @file UserService.unit.spec.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 09/21/2018
 * (c): 2018
 */

import 'jest';
import {UserService} from "./UserService";
import {createConnection} from "typeorm";
import {Container} from "inversify";

describe('UserService unit test', () => {

    let userService: UserService;
    let container = new Container();

    beforeAll((done) => {

        createConnection('test').then((connection) => {
            container.bind('Connection').toConstantValue(connection);
            userService = new UserService(connection);
            done()
        })

    });



    it('user find by phone with bad phone', () => {
        expect(userService.findByPhone('3353')).toBeFalsy();
        expect(userService.getErrors().length).toBeGreaterThan(0);
        expect(userService.getErrors()[0]).toMatchObject({
            key: 'phone',
            errors: [{key: 'format', msg: 'Wrong phone format'}]
        })
    })

});