/**
 * @file CommonUser.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 09/12/2018
 * (c): 2018
 */
import {CommonMeta} from "../CommonMeta";

export interface CommonUser extends CommonMeta {
    id: number;
    sysId: any;
    firstName: string;
    lastName: string;
    system: string;
    systemId: number;
}