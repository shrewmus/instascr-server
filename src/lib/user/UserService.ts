/**
 * @file UserService.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 08/13/2018
 * (c): 2018
 */
import {inject, injectable} from "inversify";
import {User} from "../../entity/User";
import {Connection, getRepository, Repository} from "typeorm";
import {interfaces} from "inversify-express-utils";
import {NextFunction, Request, Response} from "express";
import * as validator from 'validator';
import phone from 'phone';

@injectable()
export class UserService implements interfaces.AuthProvider {

    private _user: User;
    private repository: Repository<User>;
    private validationErrors: any[] = [];

    getErrors(): any[] {
        return this.validationErrors;
    }

    constructor(@inject('Connection') private db: Connection) {
        this.repository = db.getRepository(User);
    }

    findByPhone(phone: string) {
        // todo validate | sanitize phone
        if (this._user && this._user.phone === phone) {
            return this.resolveCachedUser();
        } else {

        }
    }

    findByEmail(email: string) {
        if (!validator.isEmail(email)) {
            return false;
        }
        if (this._user && this._user.email === email) {
            return this.resolveCachedUser();
        } else {

        }
    }

    findAll(): Promise<User[]> {
        return this.repository.createQueryBuilder('users').getMany();
    }


    getUser(req: Request, res: Response, next: NextFunction): Promise<interfaces.Principal> {
        return undefined;
    }

    private resolveCachedUser(): Promise<User> {
        return new Promise<User>((resolve, reject) => {
            resolve(this._user)
        })
    }


}