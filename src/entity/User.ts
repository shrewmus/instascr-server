import {BaseEntity, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";
import {CommonMeta} from "../lib/CommonMeta";

/**
 * @file User.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 08/12/2018
 * (c): 2018
 */

@Entity()
export class User extends BaseEntity implements CommonMeta {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column({unique: true})
    phone: string;

    @Column()
    phoneCode: string;

    @Column()
    phoneNumber: string;

    @Column()
    email: string;

    @Column()
    isEmailConfirm: boolean;

    @Column()
    isBlocked: boolean;

    @Column({type: 'jsonb'})
    metadata: any;

    @CreateDateColumn()
    dateCreated: any;

    @UpdateDateColumn()
    dateEdited: any;
}