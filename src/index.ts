///<reference path="../config/Config.d.ts"/>

import {Connection, createConnection} from "typeorm";
import {InversifyExpressServer} from "inversify-express-utils";

import MainContainer from './di.containers';
import * as cors from 'cors';
import {config} from "node-config-ts";
import * as helmet from 'helmet';

// <editor-fold desc="Import controllers (modules)">
import './lib/default/DefaultController';
import './lib/auth/AuthController';
import './lib/user/UserController';
// </editor-fold>

const port = config.port || 3000;

const server = new InversifyExpressServer(MainContainer);

createConnection()
    .then((connection) => {
        console.log('connected to db');
        MainContainer.bind<Connection>('Connection').toConstantValue(connection);
        server.setConfig((app) => {
            // enabling CORS and handling OPTIONS header also
            const corsMiddleware = cors({origin: '*', preflightContinue: true});
            app.use(corsMiddleware);
            app.use(helmet());
            app.options('*', corsMiddleware);
        });
        server.build()
            .listen(port, (err: any) => {

                if (err) {
                    return console.log('ERROR:', err);
                }

                return console.log('Server is listenning on port ' + port)
            });

    })
    .catch((error) => {
        console.log('error', error)
    });
