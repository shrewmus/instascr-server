/**
 * @file di.containers.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 08/14/2018
 * (c): 2018
 */
import {Container} from "inversify";
import {UserService} from "./lib/user/UserService";
import {AuthService} from "./lib/auth/AuthService";

const MainContainer = new Container();



// Add common services

MainContainer.bind<UserService>(UserService).toSelf();
MainContainer.bind<AuthService>(AuthService).toSelf();

export default MainContainer;